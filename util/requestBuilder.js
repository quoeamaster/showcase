
const _builder = function(token) {
  // [_token] remember the api token
  const _token = token;

  // [_cfAPIHome] the cloudflare API home | root. Currently using "v4" api(s).
  const _cfAPIHome = "https://api.cloudflare.com/client/v4";

  // [_config] prepare a config object for fetch api with Authentication headers set.
  const _config = function(method) {
    return {
      method, 
      headers: {
        "Authorization": `Bearer ${_token}`,
        "Content-Type": "application/json",
      },
    };
  };

  // return the api(s) available for the assignment
  return {
    // [obsolete] for testing purpose only, should not return the api token at whatever situation.
    // getToken: function() { return _token;},

    // [DNSRecords] return all DNS records (e.g. type A, CNAME) from the provided zone
    DNSRecords: async function(zoneID) {
      const _url = `${_cfAPIHome}/zones/${zoneID}/dns_records`;
      const _res = await fetch(_url, _config('GET'));
      // TODO: check status if necessary (e.g. 2xx is ok, 4xx and 5xx is either client or server side error)
      return await _res.json();
    },

    // [Zones] return all zones
    Zones: async function() {
      const _url = `${_cfAPIHome}/zones`;
      const _res = await fetch(_url, _config('GET'));
      return await _res.json();
    },


  };
};

// return the callable methods and properties
module.exports = {
  init: _builder,
}
/*export default {
  init: _builder,
}*/
