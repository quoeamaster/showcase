var express = require('express');
var router = express.Router();
const path = require('path');

/* GET home page. */
router.get('/', function(req, res, next) {
  // render a static html page instead
  res.sendFile(path.join(__dirname, '../', '/public/view/index.html'));
});

module.exports = router;
