var express = require('express');
var router = express.Router();
const builder = require('./../util/requestBuilder');

/* GET home page. */
router.get('/', function(req, res, next) {
  // rendering through a view template (e.g. jade or handlebars)
  res.render('index', { title: 'Express' });

  // json etc
  //res.setHeader('Content-Type', 'application/json')
  //res.send({ status: 200, message: 'testing' });
});

// [lesson] form-parsing related: https://expressjs.com/en/resources/middleware/body-parser.html
router.post('/getHeaders', function(req, res, _) {
  var content = '';

  // get the customHeaders (if any)
  var customValues = req.body.customValues;
  if (null != customValues && 0 < customValues.length) {
    content = "<b>Custom Values</b>: "+customValues+"<p/>";
  }
  // get the associated headers
  content += "<b>Associated Headers</b>: ";
  const headerKeys = Object.keys(req.headers);
  headerKeys.forEach(function(key, idx, _) {
    content += "<br/>&nbsp;&nbsp;&nbsp;" + key +" = "+req.headers[key]
  }, this);

  // add a "back" key
  content += `<p/><button onclick='window.location="/cloudflare/"'>back</button> &nbsp; <button onclick='window.location="/"'>home</button>`;
  
  res.setHeader('Content-Type', "text/html");
  res.send(content);
});

// [getDnsRecords] - api to get back the dns records based on the domain "bnerdy.icu".
router.get('/getDnsRecords', async function(req, res, _) {
  // secret is provided through the environment var (or a better secrets management tool provided by the platform etc)
  const _client = builder.init(process.env.token);
  // the target domain for the assignment
  const _domain = "bnerdy.icu";

  // get all the zone(s) first
	const _zones = await _client.Zones();
	let _zoneID = '';
	
	// filter by zone-name
	if (true == _zones.success) {
		_zones.result.forEach(function(_zone) {
			if (_domain == _zone.name) {
				_zoneID = _zone.id;
			}
		});
	}
	// if _zoneID is valid...
	if ('' == _zoneID) {
    res.setHeader('Content-Type', "text/html");
    res.status(404);
    res.send(`<h2>no Zone matched the '${_domain}'</h2>`);
	}
	// get the dns records
	const _dnsRecords = await _client.DNSRecords(_zoneID);
	if (true == _dnsRecords.success) {
    res.status(200);
    res.setHeader('Content-Type', "application/json");
    res.send(JSON.stringify(_dnsRecords.result, null, 3));
	} else {
    res.status(500);
    res.setHeader('Content-Type', "text/html");
    res.send(`failed to query for the zone's [${_domain}] DNS, error: ${_dnsRecords.errors}`);
	}
});

module.exports = router;