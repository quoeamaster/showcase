# showcase

## preparation
install [express-application-generator](https://expressjs.com/en/starter/generator.html) and generate a sample application through executing the following command:

```python
npx express-generator
```

## adding routes
A new Route should be added per product / platform basis. Take an example, an application is dedicated for cloudflare testing; hence a Router named "cloudflare" should be created. For more information, please reference the sample code - [https://gitlab.com/quoeamaster/showcase/-/blob/main/routes/cloudflare.js](https://gitlab.com/quoeamaster/showcase/-/blob/main/routes/cloudflare.js).

## cloudflare route and api endpoints
For the cloudflare showcase, there are currently 2 endpoints:
  - / : the landing page providing a textarea for entering form-value(s); all such data would be posted to the server side through clicking the "execute" button.
  - /getHeaders : a dynamic page returning the associated http request headers + any custom form value(s) provided.

In order to retrieve the request headers, simply access the request object's __headers__ properties. To iterate through the provided headers, we would need to get the keys of the headers properties by:

```javascript
Object.keys(req.headers);
```

Once we have the keys on hand, simply iterate through the __forEach()__ function of the keys and access the value by:
```javascript
req.headers(key);
```

To access the form-values; we would need to make sure the __Express__ app has setup the body-parser correctly as the middleware.
```javascript
const bodyparser = require('body-parser');
...
// extended = false means the parsed data of a json would only be either string or array type;
// extended = true means the parsed data of a json could be any valid type (eg. numbers, strings, date...)
app.use(bodyParser.urlencoded({ extended: false }));
```

After the setting is done, simply run the following to access the form-values:
```javascript
// trying to access the form-value associated with an html text/textarea/combobox/option named "form-object-name"
req.body['form-object-name'];
```


